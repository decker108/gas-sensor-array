# gas-sensor-array

The firmware for a circuit consisting of three FC-22 cards holding an MQ-4,
MQ-6 and MQ-7 gas sensor, all connected to an Lolin C3 Mini (RISCV) board. 
These sensors are read periodically and sent to a remote server for storage and histogram visualization.

## First time setup

- build code with `pio run`
- connect c3-mini board to usb
- hold 9 button on board pressed
- press and release rst button
- release 9 button
- upload with `pio run -t upload`
- when upload is done, press and release rst button

## Note

The board might not use /dev/ttyUSB0 but instead /dev/ttyACM1 for
connections.

## Circuit diagram

![Alt text](./circuit.png "Circuit diagram")