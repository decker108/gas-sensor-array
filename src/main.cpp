#include <Arduino.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>

#define LED 7

String lpgPrefix = "lpg: ";
String lngPrefix = " lng: ";
String coPrefix = " co: ";

char ssid[] = "my-ssid";
char pass[] = "my-wifi-pwd";

unsigned long previousMillis = 0;
const long interval = 600000; // 10 minutes

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(0, INPUT);
  pinMode(1, INPUT);
  pinMode(2, INPUT);
  Serial.begin(115200);
  delay(1000);

  Serial.println("Connecting...");
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  WiFi.begin(ssid, pass);

  WiFi.setTxPower(WIFI_POWER_8_5dBm);

  while (WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    delay(500);
  }

  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());
}

void sendMeasurements(int lpgValue, int lngValue, int coValue) {
  Serial.println("Begin sending measurements");
  Serial.println("Wifi-status: " + String(WiFi.status()) + " " + String(WiFi.RSSI()));

  const char* host = "rpi3b.local";
  WiFiClient client;
  const int httpPort = 3030;
  if (!client.connect(host, httpPort)) {
      Serial.println("connection failed");
      return;
  }

  // We now create a URI for the request
  String url = "/measurements/record";

  Serial.print("Requesting URL: ");
  Serial.println(url);

  String output = "";
  StaticJsonDocument<48> doc;
  doc["lpg"] = lpgValue;
  doc["lng"] = lngValue;
  doc["co"] = coValue;
  serializeJson(doc, Serial);
  serializeJson(doc, output);
  Serial.println(String("Body length: ") + String(output.length()));

  // This will send the request to the server
  client.print(String("POST ") + url + " HTTP/1.1\r\n" +
                "Host: " + host + "\r\n" +
                "Connection: close\r\n" +
                "Content-Type: application/json\r\n" +
                "Content-Length: " + output.length() + "\r\n\r\n" +
                output);
  unsigned long timeout = millis();
  while (client.available() == 0) {
      if (millis() - timeout > 5000) {
          Serial.println(">>> Client Timeout !");
          client.stop();
          return;
      }
  }

  // Read all the lines of the reply from server and print them to Serial
  while(client.available()) {
      String line = client.readStringUntil('\r');
      Serial.print(line);
  }
  Serial.println();

  Serial.println("Completed sending measurements");
}

void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;

    digitalWrite(LED, 1);
    int coValue = analogRead(0); // co (mq7)
    int lpgValue = analogRead(1); // lpg (mq6)
    int lngValue = analogRead(2); // lng (mq4)
    sendMeasurements(lpgValue, lngValue, coValue);
    digitalWrite(LED, 0);
  }
}
